async function readfile(filePath) {
    try {
        const data = await fs.readFile(filePath);
        console.log(data.toString());
    } catch (error) {
        console.error(`Got an error trying to read the file:{error.message}`);
    }
}

const moveFrom = '';
const moveTo = '';

fs.readdir(moveFrom, function(err, files){
    if (err) {
        console.error('could not list the directory', err);
        process.exit(1);
    }
    files.forEach(function (file, index){

        //make one pass and make the file complete
        const fromPath = path.join(moveFrom, file);
        const toPath = path.join(moveTo, file);

        fs.stat(fromPath, function(error, stat){
            if(error) {
                console.error('Error stating file', error);
                return;
            }
            if (stat.isFile())
                console.log("'%s', is a file.", fromPath);
            else if (stat.isDirectory())
                console.log("'%s', is a directory.", fromPath);

            fs.rename(fromPath, toPath, function(error){
                if(error){
                    console.error('File moving error', error);
                } else {
                    console.log("Moved file '%s' to '%s'.", fromPath, toPath);
                }
            });
        });
    });
});

async function getFiles(pathName){
    try {

        // const files = await fs.promises.readdir(pathName);
        const dir = await fs.promises.opendir(pathName);
        
        for await (const dirent of dir) {
            console.log(dirent.name);
        }
    } catch (e) {
        console.error("we've thrown! Whoolps!", e);
    }
}