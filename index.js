const path = require('path');
const sizeOf = require('image-size');
const http = require('http');
const express = require('express');
const fs = require('fs');
const sharp = require('sharp');
const cors = require('cors')
const app = express();
app.use(cors())
app.use(express.json())
const IMAGE_EXTENTIONS = ['jpg', 'png', 'gif', 'jpeg', 'bmp', 'heic'];

function isImage(filename) {
    const ext = filename.split('.').slice(-1)[0];
    if (ext === filename) {
        return false;
    }
    if (IMAGE_EXTENTIONS.indexOf(ext.toLowerCase()) === -1) {
        return false;
    }
    return true;
}
app.get('/lista', function(req, res) {
    const dirname = req.query.id;
    var arrfiles = [];
    fs.readdir(dirname, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            files.forEach(function(file) {
                if (isImage(file)) {
                    arrfiles.push(fs.readFileSync(`${dirname}/${file}`, 'utf-8'));
                }
            });
        }
        res.json({ data: arrfiles });
    });
});
app.get('/listaimagens', function(req, res) {
    try {
        let path;
        const dirname = req.query.id;
        const screenwidth = req.query.w;
        const screenheight = req.query.h;
        const arrfile = [];
        let indice = 0;
        fs.readdir(dirname, (err, files) => {
            files.forEach(file => {
                if ( isImage(file) ) {
                    path = dirname + file;
                    var dimensions = sizeOf(path);
                    var stats = fs.statSync(path);
                    var fileSizeInBytes = stats.size;
                    arrfile.push({
                        indice: indice,
                        src: file,
                        originalWidth: dimensions.width,
                        originalHeight: dimensions.height,
                        screenWidth: Number(screenwidth),
                        screenHeight: (Number(screenheight) - 100),
                        resizedWidth: ((Number(dimensions.width) / Number(dimensions.height)) * (Number(screenheight) - 100)),
                        fileSizeInBytes: fileSizeInBytes
                    });
                    indice++;
                }
            });
            res.json({ data: arrfile });
        });
    } catch (err) {
        res.json(err);
    }
})
app.get('/', (request, response) => {
    response.send("home")
})
app.listen(8080, function(){
    console.log('===============================')
    console.log('aplicação rodando na porta 8080')
    console.log('===============================')
});
